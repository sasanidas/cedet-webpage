;;; wsimple-template.el --- Simple template system             -*- lexical-binding: t; -*-

;; Copyright (C) 2021 Fermin Munoz

;; Author: Fermin Munoz

;; Maintainer: Fermin Munoz <fmfs@posteo.net>
;; License: GPL-3.0-or-later

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Quick intro

;;; Code:

;;;; The requires
(require 'seq)
(require 'project)



(defvar wsimple-template-directory "templates")

(defvar wsimple-template-output-directory "public")

;;(rx bol "<!--<<" (group (+ alnum) ".html") ">>-->" eol)
(defvar wsimple-template-regex "^<!--<<\\([[:alnum:]]+\\.html\\)>>-->$")


(defun wsimple-template-goto-template-source ()
  "Go to the template file at point."
  (interactive)
  (let* ((file-name (thing-at-point 'symbol))
	 (file-path (expand-file-name file-name (expand-file-name wsimple-template-directory "."))))
    (if (file-exists-p file-path)
	(find-file file-path)
      (error (format "The file %s cannot be found in %s" file-name wsimple-template-directory)))))


(defun wsimple-template-generate-project ()
  "Get all the files from the current `project-roots' directory.
And output the results to `wsimple-template-output-directory'."
  (interactive)
  (let* ((current-root (car (project-roots (project-current))))
	 (html-files (seq-filter (lambda (file)
				   (string= (file-name-extension file) "html"))
				 (directory-files current-root))))
    (seq-map (lambda (file)
	       (message "Generating .html file from %s" file)
	       (wsimple-template-generate-file (expand-file-name file current-root)))
	     html-files)))


(defun wsimple-template-generate-file (&optional file)
  "From the current buffer generate an .html file with templates.
Optionally, it can take another file from FILE."
  (interactive)
  (let* ((regex wsimple-template-regex)
	 (current-file (if file file (buffer-file-name)))
	 (otp-directory wsimple-template-output-directory)
	 (file-name (file-name-base current-file))
	 (buffer-content (if file
			     (with-temp-buffer
			       (insert-file-contents current-file)
			       (buffer-string))
			   (buffer-string)))
	 (match nil))
    (with-temp-buffer
      (insert buffer-content)
      (goto-char (point-min))
      (while (re-search-forward regex nil t)
	(setq match (match-string 1))
	(replace-match (with-temp-buffer
			 (insert-file-contents
			  (format "%s/%s" wsimple-template-directory match))
			 (buffer-string))))
      (write-file (format "%s/%s.html"
			  otp-directory
			  file-name)))))

(provide 'wsimple-template)
;;; wsimple-template.el ends here
