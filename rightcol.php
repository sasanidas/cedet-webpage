<table align="right" cellspacing="0" cellpadding="0" class="SPEEDBAR"><tr><td>
    <table bgcolor="white" cellspacing="0" cellpadding="1" width="220">

	<tr><td align="center">&nbsp<a href="/"><b><font size="+1">CEDET</font></b></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<b>User Tools</b></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="setup.shtml"><b>Simple Setup</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="projects.shtml"><b>Project Management</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="intellisense.shtml"><b>Smart Completion</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="symref.shtml"><b>Find References</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="codegen.shtml"><b>Code Generation</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="uml.shtml"><b>UML Graphs</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="languagesupport.shtml"><b>Language Support</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="projectsupport.shtml"><b>Project Support</b></a></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<b>Developer Primers</b></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="addlang.shtml"><b>Add a Language</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="addtool.shtml"><b>Add external tool</b></a></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<b>Parts of CEDET</b></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="ede.shtml"><b>EDE</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="semantic.shtml"><b>Semantic</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="srecode.shtml"><b>SRecode</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="cogre.shtml"><b>Cogre</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="speedbar.shtml"><b>Speedbar</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="eieio.shtml"><b>EIEIO</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="misc.shtml"><b>Misc Tools</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://randomsample.de/cedetdocs"><b>Documentation</b></a></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<a class="SB" href="http://sourceforge.net/project/showfiles.php?group_id=17886"><b>Releases</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="https://sourceforge.net/projects/cedet/files/cedet/cedet-1.0.1.tar.gz/download"><b>1.0.1</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="https://sourceforge.net/projects/cedet/files/cedet/cedet-1.1.tar.gz/download"><b>1.1</b></a></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<a class="SB" href="http://www.sourceforge.net"><b>Source Forge</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://www.sourceforge.net/projects/cedet"><b>Project</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://sourceforge.net/mail/?group_id=17886"><b>Mailing Lists</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp&nbsp<img src="tag.png">&nbsp<a class="SB" href="http://lists.sourceforge.net/lists/listinfo/cedet-devel"><b><font size="-1">cedet-devel</font></b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp&nbsp<img src="tag.png">&nbsp<a class="SB" href="http://lists.sourceforge.net/lists/listinfo/cedet-semantic"><b><font size="-1">cedet-semantic</font></b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp&nbsp<img src="tag.png">&nbsp<a class="SB" href="http://lists.sourceforge.net/lists/listinfo/cedet-eieio"><b><font size="-1">cedet-eieio</font></b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="https://sourceforge.net/project/screenshots.php?group_id=17886"><b>Screenshots</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="git-repo.shtml"><b>Git Repository</b></a></a>&nbsp</td></tr>
	<tr><td class="BAR"><img src="dir-minus.gif">&nbsp<b>More Tools</b></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://jdee.sf.net"><b>JDEE</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://ecb.sf.net"><b>ECB</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp<img src="page.gif">&nbsp<a class="SB" href="http://www.dr-qubit.org/emacs.php"><b>CompletionUI</b></a></a>&nbsp</td></tr>
	<tr><td >&nbsp</td></tr>

	<tr><td class="BAR">

	    <table cellspacing="0" cellpadding="1" width="100%"><tr><td align="left">&lt;&lt;</td>
		<td align="center"><a href="ftpgate.shtml">Files</a></td>
		<td align="right">&gt;&gt;</td>
	    </tr></table>

	</td></tr>
    </table>
</td></tr></table>
