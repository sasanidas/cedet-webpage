<!-- -*-html-*- -->
<br clear="all">
<p>
<table width="100%" class="BAR"><tr><td>
<h2>Downloading CEDET</h2>
</td></tr></table>

<p>All the <b>CEDET</b> tools are available from a single distribution
  file to ease installation.</p>

<h3>Latest Stable Release: CEDET 1.1</h3>

<p>Try out
<a href="https://sourceforge.net/projects/cedet/files/cedet/cedet-1.1.tar.gz/download">
cedet-1.1.tar.gz</a>.

<p>CEDET 1.1 includes all the security fixes from 1.0.1, a long list
of bug fixes, and additional new features to support Java, Android and
Arduino programming!

<p>Our goal for CEDET 1.1 is that this will be the <b>LAST</b> release using
  the current install and file organization schemes.  This will
  also be the last release to support Emacs 22!  Future released
  versions will use a new file system scheme compatible with Emacs 24.<

<P>After building <b>CEDET 1.1</b>, consider joining the
<a href="http://lists.sourceforge.net/lists/listinfo/cedet-devel">mailing
list</a> and help make CEDET better.


<h3>Previous Stable Release: CEDET 1.0.1</h3>

<p>The previous stable release is
<a href="https://sourceforge.net/projects/cedet/files/cedet/cedet-1.0.1.tar.gz/download">
cedet-1.0.1.tar.gz</a>.  You will need CEDET 1.0.1 for older versions
of Emacs, such as Emacs 21 or 22.1.

<p><b>About this Security Release:</b><br>
CEDET 1.0 has a security issue related to loading project files with
EDE.  If you are using CEDET 1.0, please upgrade to CEDET 1.0.1 to
recover from this issue.  If you use Emacs 23.3 with built-in CEDET,
please upgrade to Emacs 23.4 or later, apply the
<a href="http://thread.gmane.org/gmane.emacs.devel/147499">patch</a>,
or upgraded to <a href="https://sourceforge.net/projects/cedet/files/cedet/cedet-1.0.1.tar.gz/download">CEDET 1.0.1</a>.


<h3>CEDET Development</h3>

<p><b>Did you find a bug?</b>

<p>If you encounter problems with a CEDET release, those issues
  may have already been fixed in Git!  CEDET has an active community
  of users that help identify and fix these issues quickly.  You can check the
  <a href="https://sourceforge.net/mailarchive/forum.php?forum_name=cedet-devel">
  mailing list archives</a> or just try the
  <a href="git-repo.shtml">
  Git version</a> directly.

<p>When using CEDET from Git, please note that we are transitioning
  to a new file and install scheme to be more compatible with Emacs
  24.  Changes will be required in your <tt>.emacs</tt>.

<p><b>Emacs Version Support:</b>

<p>CEDET 1.0.1, and 1.1* has TWO automated build processes, one via
Make, and the other via starting Emacs, and executing a build command.

<p>This table shows the <a href="http://www.randomsample.de:4455/">automated testing</a>
done for the release of CEDET 1.1:</p>

<table>
<tr><th>&nbsp</th><th>Emacs 22</th><th>Emacs 23</th><th>Emacs 24</th></tr>

<tr align=center><th>UBuntu 11.10</th>
<td></td><td><font color=green>&#x2713;</font></td><td></td>
</tr>
<tr align=center><th>Debian Stable</th>
<td><font color=green>&#x2713;</font></td><td><font color=green>&#x2713;</font></td><td><font color=green>&#x2713;</font></td>
</tr>
<tr align=center><th>OSX 10.6</th>
<td></td><td><font color=green>&#x2713;</font></td><td><font color=green>&#x2713;</font></td>
</tr>
<tr align=center><th>Windows 7/cygwin</th>
<td></td><td><font color=green>&#x2713;</font></td><td></td>
</tr>
</table>

<p><b>Emacs 21</b>
  CEDET's test suite will fail, but most parts still work for CEDET 1.0.1.

<p><b>XEmacs</b>
  Neither build process works with XEmacs 21.4.  It is possible to
  build parts of it by hand it so it works however.

<p><b>SXEmacs</b>
  We have integrated patches to support SXEmacs, but we haven't tested
  it ourselves.  Parts of CEDET have been reported to install and work.

<p><b>Windows XP/7</b>
  If you do not have cygwin, you will need to use the <tt>cedet-build.el</tt>
  script to build CEDET.
